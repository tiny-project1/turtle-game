from turtle import Turtle

STARTING_POSITION = (0, -280)
MOVE_DISTANCE = 10
FINISH_LINE_Y = 280


class Player(Turtle):
    """Manage Player"""
    def __init__(self):
        super().__init__()
        self.shape("turtle")
        self.penup()
        self.go_to_start()
        self.setheading(90)

    def move_up(self):
        """Make the player move"""
        self.forward(MOVE_DISTANCE)

    def go_to_start(self):
        """Afer reset the game, set position of the player at the start"""
        self.goto(STARTING_POSITION)

    def finish_line(self):
        """Check if the player reach the end"""
        if self.ycor() > FINISH_LINE_Y:
            return True
        return False
