import time
import turtle
from turtle import Screen

from car_manager import CarManager
from player import Player
from scoreboard import Scoreboard

screen = Screen()
screen.setup(width=600, height=600)
screen.tracer(0)
turtle.listen()


player = Player()
car_manager = CarManager()
scoreboard = Scoreboard()

turtle.onkeypress(key="Up", fun=player.move_up)

game_is_on = True
while game_is_on:
    time.sleep(0.1)
    screen.update()

    car_manager.create_cars()
    car_manager.move_cars()

    for car in car_manager.all_car:
        if car.distance(player) < 20:
            scoreboard.game_over()
            game_is_on = False

    if player.finish_line():
        player.go_to_start()
        car_manager.level_up()
        scoreboard.increase_level()
        scoreboard.update_level()


screen.exitonclick()
