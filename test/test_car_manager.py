from turtle import Turtle
from turtlegame.car_manager import CarManager


def test_create_cars(monkeypatch):
    monkeypatch.setattr('random.randint', lambda a, b: 1)

    car_manager = CarManager()
    initial_length = len(car_manager.all_car)

    car_manager.create_cars()

    assert len(car_manager.all_car) == initial_length + 1


def test_not_create_cars(monkeypatch):
    monkeypatch.setattr('random.randint', lambda a, b: 2)
    car_manager = CarManager()
    car_manager.create_cars()

    assert len(car_manager.all_car) == 0

def test_move_cars():
    car_manager = CarManager()
    car = Turtle()
    car_manager.all_car.append(car)
    car_manager.move_cars()
    assert car.xcor() == -5  # car_speed est initialisé à 5


def test_level_up():
    car_manager = CarManager()
    car_manager.level_up()
    assert car_manager.car_speed == 15  # MOVE_INCREMENT est initialisé à 5
