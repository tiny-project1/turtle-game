from turtlegame.player import Player, STARTING_POSITION, MOVE_DISTANCE, FINISH_LINE_Y


def test_move_up():
    player = Player()
    initial_y_position = player.ycor()
    player.move_up()
    assert player.ycor() == initial_y_position + MOVE_DISTANCE


def test_go_to_start():
    player = Player()
    player.goto(50, 50)
    player.go_to_start()
    assert player.position() == STARTING_POSITION


def test_finish_line_above():
    player = Player()
    player.goto(0, FINISH_LINE_Y + 10)
    assert player.finish_line() == True


def test_finish_line_below():
    player = Player()
    player.goto(0, FINISH_LINE_Y - 10)
    assert player.finish_line() == False
