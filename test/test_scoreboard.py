from unittest.mock import MagicMock
from turtlegame.scoreboard import Scoreboard


def test_initial_scoreboard_level():
    scoreboard = Scoreboard()
    assert scoreboard.level == 1


def test_increase_level():
    scoreboard = Scoreboard()
    scoreboard.increase_level()
    assert scoreboard.level == 2


def test_update_level():
    scoreboard = Scoreboard()
    scoreboard.write = MagicMock()
    scoreboard.update_level()
    scoreboard.write.assert_called_once()  # Vérifie si la méthode write() a été appelée


def test_game_over():
    scoreboard = Scoreboard()
    scoreboard.write = MagicMock()
    scoreboard.game_over()
    scoreboard.write.assert_called_once_with(
        f"GAME OVER ! You reached level : {scoreboard.level}",
        align="center",
        font=("Courier", 18, "normal")
    )
