import random
from turtle import Turtle

COLORS = ["red", "orange", "yellow", "green", "blue", "purple"]
STARTING_MOVE_DISTANCE = 5
MOVE_INCREMENT = 10


class CarManager(Turtle):
    """Manage the cars"""
    def __init__(self):
        self.all_car = []
        self.car_speed = STARTING_MOVE_DISTANCE

    def create_cars(self, random_chance=random.randint(1, 6)):
        """Create a car with a ratio 1/6 and add it to the list of car"""
        random_chance = random.randint(1, 6)
        if random_chance == 1:
            new_car = Turtle(shape="square")
            new_car.color(random.choice(COLORS))
            new_car.shapesize(stretch_wid=1, stretch_len=2)
            new_car.penup()
            new_car.goto(x=300, y=random.randint(-250, 250))
            self.all_car.append(new_car)

    def move_cars(self):
        """Move the car"""
        for car in self.all_car:
            car.backward(self.car_speed)

    def level_up(self):
        """Every time the player end the game the car's speed increase"""
        self.car_speed += MOVE_INCREMENT
