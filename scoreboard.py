from turtle import Turtle

FONT = ("Courier", 18, "normal")
ALIGNEMENT = "center"


class Scoreboard(Turtle):
    """Manage the Scoreboard"""
    def __init__(self):
        super().__init__()
        self.hideturtle()
        self.level = 1
        self.color("black")
        self.penup()
        self.goto(x=-220, y=270)
        self.update_level()

    def game_over(self):
        """Write a text when the player loose"""
        self.goto(x=0, y=0)
        self.write(
            f"GAME OVER ! You reached level :"
            f" {self.level}", align=ALIGNEMENT, font=FONT
        )

    def increase_level(self):
        """Increase the level (text) when player finish the game"""
        self.level += 1
        self.update_level()

    def update_level(self):
        """Update the level on screer"""
        self.clear()
        self.write(f"Level : {self.level}", align=ALIGNEMENT, font=FONT)
